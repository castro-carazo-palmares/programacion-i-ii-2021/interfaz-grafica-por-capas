package Datos;

public class Carro {
    private String placa;
    private String marca;
    private String modelo;
    private int anio;
    private String color;
    private String tipo;

    public Carro() {
        this("", "", "", 0, "", "");
    }

    public Carro(String placa, String marca, String modelo, int anio, String color, String tipo) {
        this.placa = placa;
        this.marca = marca;
        this.modelo = modelo;
        this.anio = anio;
        this.color = color;
        this.tipo = tipo;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public int getAnio() {
        return anio;
    }

    public void setAnio(int anio) {
        this.anio = anio;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String guardar() {
        return placa + ";" + marca + ";" + modelo + ";" + anio + ";" + color + ";" + tipo + "\n";
    }

    @Override
    public String toString() {
        return "Placa: " + placa + "\n" +
                "Marca: " + marca + "\n" +
                "Modelo: " + modelo + "\n" +
                "Año: " + anio + "\n" +
                "Color: " + color + "\n" +
                "Tipo: " + tipo + "\n";
    }
}

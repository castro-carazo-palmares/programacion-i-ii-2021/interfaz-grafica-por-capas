package Presentacion;

import Datos.Carro;
import Negocio.Lectura;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class PantallaPrincipal {
    private JPanel panel;
    private JButton btnAgregar;
    private JButton btnMostrar;
    private JButton btnSalir;
    private Lectura lector;

    public PantallaPrincipal() {
        JFrame frame = new JFrame("Pantalla Principal");
        frame.setContentPane(panel);
        frame.setLayout(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(300, 325);
        frame.setResizable(false);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

        lector = new Lectura();

        btnAgregar.setBounds(65, 50, 150, 50);
        btnMostrar.setBounds(65, 125, 150, 50);
        btnSalir.setBounds(65, 200, 150, 50);

        btnAgregar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new Formulario();
            }
        });

        btnMostrar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                lector.abrir();
                Carro[] vectorCarros = lector.leer();
                lector.cerrar();

                for (int indice = 0; indice < vectorCarros.length; indice++) {
                    if(vectorCarros[indice] != null) {
                        JOptionPane.showMessageDialog(null, vectorCarros[indice].toString());
                    }
                }
            }
        });

        btnSalir.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
    }

    public static void main(String[] args) {
        new PantallaPrincipal();
    }
}

package Presentacion;

import Datos.Carro;
import Negocio.Escritura;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Formulario {
    private JPanel panel;
    private JTextField txtPlaca;
    private JTextField txtMarca;
    private JTextField txtModelo;
    private JTextField txtAnio;
    private JTextField txtColor;
    private JTextField txtTipo;
    private JLabel lblPlaca;
    private JLabel lblMarca;
    private JLabel lblModelo;
    private JLabel lblAnio;
    private JLabel lblColor;
    private JLabel lblTipo;
    private JButton btnGuardar;
    private Escritura escritor;

    public Formulario() {
        JFrame frame = new JFrame("Formulario");
        frame.setContentPane(panel);
        frame.setLayout(null);
        frame.setSize(500, 600);
        frame.setResizable(false);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

        escritor = new Escritura();

        lblPlaca.setBounds(110, 40, 70, 50);
        txtPlaca.setBounds(180, 40, 200, 50);

        lblMarca.setBounds(110, 100, 70, 50);
        txtMarca.setBounds(180, 100, 200, 50);

        lblModelo.setBounds(110, 160, 70, 50);
        txtModelo.setBounds(180, 160, 200, 50);

        lblAnio.setBounds(110, 220, 70, 50);
        txtAnio.setBounds(180, 220, 200, 50);

        lblColor.setBounds(110, 280, 70, 50);
        txtColor.setBounds(180, 280, 200, 50);

        lblTipo.setBounds(110, 340, 70, 50);
        txtTipo.setBounds(180, 340, 200, 50);

        txtPlaca.requestFocus();

        btnGuardar.setBounds(110, 450, 270, 50);

        btnGuardar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(validar()) {
                    String placa = txtPlaca.getText();
                    String marca = txtMarca.getText();
                    String modelo = txtModelo.getText();
                    int anio = Integer.parseInt(txtAnio.getText());
                    String color = txtColor.getText();
                    String tipo = txtTipo.getText();

                    Carro carro = new Carro(placa, marca, modelo, anio, color, tipo);

                    escritor.abrir();
                    boolean guardado = escritor.escribir(carro);
                    escritor.cerrar();

                    if(guardado) {
                        JOptionPane.showMessageDialog(null, "El carro se guardó correctamente");
                        limpiar();
                    } else {
                        JOptionPane.showMessageDialog(null, "No se pudo guardar la información, intente de nuevo");
                    }
                }
            }
        });
    }

    public boolean validar() {
        if (!txtPlaca.getText().equals("") &&
                !txtMarca.getText().equals("") &&
                !txtModelo.getText().equals("") &&
                !txtAnio.getText().equals("") &&
                !txtColor.getText().equals("") &&
                !txtTipo.getText().equals("")) {
            return true;
        }
        return false;
    }

    public void limpiar() {
        txtPlaca.setText("");
        txtMarca.setText("");
        txtModelo.setText("");
        txtAnio.setText("");
        txtColor.setText("");
        txtTipo.setText("");
        txtPlaca.requestFocus();
    }
}

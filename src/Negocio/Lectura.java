package Negocio;

import Datos.Carro;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Lectura {
    private BufferedReader lector;

    public void abrir() {
        try {
            lector = new BufferedReader(new FileReader("carros.txt"));
        } catch (FileNotFoundException fileNotFoundException) {
            System.err.println("No se pudo encontrar el archivo.");
        }
    }

    public Carro[] leer() {
        Carro carro;
        Carro[] vectorCarros = new Carro[10];
        int indice = 0;

        try {
            String linea = lector.readLine();
            String[] datos;

            while(linea != null){
                carro = new Carro();
                datos = linea.split(";");
                carro.setPlaca(datos[0]);
                carro.setMarca(datos[1]);
                carro.setModelo(datos[2]);
                carro.setAnio(Integer.parseInt(datos[3]));
                carro.setColor(datos[4]);
                carro.setTipo(datos[5]);

                vectorCarros[indice] = carro;
                indice++;
                linea = lector.readLine();
            }
        } catch (IOException ioException) {
            System.err.println("No se pudo leer el archivo.");
        }

        return vectorCarros;
    }

    public void cerrar() {
        try {
            if(lector != null) {
                lector.close();
            }
        } catch (IOException ioException) {
            System.err.println("Se produjo un error al cerrar el archivo.");
        }
    }

}

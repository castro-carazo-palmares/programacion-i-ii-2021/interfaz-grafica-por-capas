package Negocio;

import Datos.Carro;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class Escritura {
    private BufferedWriter escritor;

    public void abrir() {
        try {
            escritor = new BufferedWriter(new FileWriter("carros.txt", true));
        } catch (IOException ioException) {
            System.err.println("Se produjo un error al crear el archivo.");
        }
    }

    public boolean escribir(Carro carro) {
        try {
            escritor.write(carro.guardar());
            return true;
        } catch (IOException ioException) {
            System.err.println("Se produjo un error al escribir en el archivo.");
            return false;
        }
    }

    public void cerrar() {
        try {
            if (escritor != null) {
                escritor.close();
            }
        } catch (IOException ioException) {
            System.err.println("Se produjo un error al cerrar el archivo.");
        }
    }

}
